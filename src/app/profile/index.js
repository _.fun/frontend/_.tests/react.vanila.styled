import React from 'react'

import { Box } from './containers'
import Theme from './theme'

import { list } from 'theme'

const Profile = () => (
  <Box>
    <Theme list={list} />
  </Box>
)

export default Profile
