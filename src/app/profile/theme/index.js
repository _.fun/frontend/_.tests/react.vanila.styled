import React from 'react'
import { Button } from 'app/common'

const Theme = ({ list, changeThemeAction }) => (
  <div>
    <h1>themes</h1>
    {list.map(({ name }) => (
      <Button
        isAccent
        isCircular
        isHollow
        key={name}
        onClick={() => changeThemeAction(name)}
      >
        {name}
      </Button>
    ))}
  </div>
)

export default Theme
