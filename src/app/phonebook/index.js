import React, { Component } from 'react'

import { Grid } from './containers'
import Search, { SearchPropTypes } from './search'
import Action, { ActionPropTypes } from './action'
import List, { ListPropTypes } from './list'

class Phonebook extends Component {
  constructor(props) {
    super(props)

    this.state = {
      loading: false,
      firstLoad: false,
      search: '',
      list: []
    }
  }

  componentDidMount() {
    const { firstLoad } = this.state
    if (firstLoad) this.firstLoadAction()
  }

  firstLoadAction() {
    // do something
  }

  searchAction() {
    // do something
  }

  clearSearchAction() {
    // do something
  }

  render() {
    const { list, search, loading } = this.state

    return (
      <Grid>
        <Search search={search} searchAction={this.searchAction} />
        <Action clearSearchAction={this.clearSearchAction} />
        <List list={list} loading={loading} />
      </Grid>
    )
  }
}

Phonebook.propTypes = {
  ...ListPropTypes,
  ...SearchPropTypes,
  ...ActionPropTypes
}

export default Phonebook
